def call(body) {
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    node() {
        stage('Checkout') {
            checkout scm
        }
        stage('Build') {
            echo 'TBD: Building ...'
        }
        stage('UnitTest') {
            echo 'TBD: Running unit tests ...'
        }
		stage('Publish') {
            echo 'TBD: Running unit tests ...'
        }
    }
}